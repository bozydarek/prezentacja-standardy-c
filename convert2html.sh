#!/bin/bash

name="CppStandards"

jupyter-nbconvert presentation.ipynb --to slides --reveal-prefix=./lib/reveal.js --output=$name

name=$name".slides.html"

sed -i -e 's,https://cdnjs.cloudflare.com/ajax/libs/require.js/2.1.10,./lib,g' $name
sed -i -e 's,https://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3,./lib,g' $name
sed -i -e 's,//netdna.bootstrapcdn.com/font-awesome/4.1.0,./lib/font-awesome-4.7.0,g' $name
sed -i -e 's,https://cdn.mathjax.org/mathjax/latest,./lib/MathJax-2.7.1,g' $name


