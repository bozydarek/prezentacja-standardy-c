#include <iostream>
#include <functional>

int foo(int i) {return i+1;}


struct S {
    int j = 5;
    int foo(int i) {return i+j;};
};

int main()
{
  std::cout<<std::invoke(foo, 5)<<std::endl;
  S s;

  std::cout<<std::invoke(&S::foo, s, 5)<<std::endl;
  return 0;
}
