#include <iostream>

template<typename ... T>
auto sum(T ... t)
{
  (std::cout << ... << t) << '\n';
  return ( t + ...);
}

int main()
{
  std::cout<<sum(1,2,3,4,5)<<"\n";
}
