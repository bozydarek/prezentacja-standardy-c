#include <iostream>
#include <experimental/filesystem>
namespace fs = std::experimental::filesystem;
int main()
{
    std::cout << "Current path is " << fs::current_path() << '\n';
    for(auto& p: fs::directory_iterator(fs::current_path()))
        std::cout << p << '\n';
}
