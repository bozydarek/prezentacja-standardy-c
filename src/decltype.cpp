#include <iostream>

using namespace std;

int main()
{
    int a = 1;
    decltype(a) b;
    //auto c;    
    auto c = a; 
    
    decltype((a)) r = a;
    ++a;    

    cout << "A:" << a << " C:" << c << " R:" << r << endl;

    b = 3.1415;
    decltype(a) d = 2.7183;    

    cout << "B: " << b << " D:" << d << endl;

return 0;
}
