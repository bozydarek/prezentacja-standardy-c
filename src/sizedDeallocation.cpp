#include <iostream>
#include <chrono>

#define POW 1000000
#define SIZE 10000

using namespace std;

struct X {

	int foo;
	double bar;

    static void operator delete(void* ptr, std::size_t sz)
    {
        //std::cout << "custom delete for size " << sz << '\n';
        ::operator delete(ptr, sz);
    }
    static void operator delete[](void* ptr, std::size_t sz)
    {
        //std::cout << "custom delete for size " << sz << '\n';
        ::operator delete(ptr, sz);
    }
};

struct Y {

	int foo;
	double bar;
	
	static void operator delete(void* ptr)
    {
        //std::cout << "normal delete \n";
        ::operator delete(ptr);
    }
    static void operator delete[](void* ptr)
    {
        //std::cout << "normal delete \n";
        ::operator delete(ptr);
    }
};

int main()
{
	// (5-6) Called instead of (1-2) if a user-defined replacement is provided 
	// except that it's implementation-defined whether (1-2) or (5-6) is called 
	// when deleting objects of incomplete type and arrays of non-class and trivially-destructible 
	// class types (since C++17). A memory allocator can use the given size to be more efficient. 
	
	// The standard library implementations are identical to (1-2).


	std::chrono::time_point<std::chrono::system_clock> start, end;
	start = std::chrono::system_clock::now();
	
		for(int i=0; i<POW; ++i)
		{
			 X* p = new X[SIZE];
			 delete[] p;
		}
	
	end = std::chrono::system_clock::now();

	std::chrono::duration<double> elapsed_seconds = end-start;

	cout << elapsed_seconds.count() << endl;

	/// ----------------------------------------
	
	start = std::chrono::system_clock::now();
	
		for(int i=0; i<POW; ++i)
		{
			 Y* p = new Y[SIZE];
			 delete[] p;
		}
	
	end = std::chrono::system_clock::now();

	elapsed_seconds = end-start;

	cout << elapsed_seconds.count() << endl;


return 0;
}
