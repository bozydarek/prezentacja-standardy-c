#include <iostream>

using namespace std;

int main(){

    int x = 1;

    auto getX = [=]{ return x; };
    getX(); // == 1

    auto addX = [=](int y) { return x + y; };
    addX(1); // == 2

    auto getXRef = [&]() -> int& { return x; };
    getXRef(); // int& to `x`
 



    auto f1 = [&x] { x = 2; }; // OK: x is a reference and modifies the original
    //auto f2 = [x] { x = 2; }; // ERROR: the lambda can only perform const-operations on the captured value

    auto f3 = [x] () mutable { x = 42; cout << "scope:" << x << endl; }; 
    // OK: the lambda can perform any operations on the captured value
 
    cout << "X:" << x << endl;

    f1();
    cout << "X:" << x << endl;

    f3();
    cout << "X:" << x << endl;

return 0;
}
