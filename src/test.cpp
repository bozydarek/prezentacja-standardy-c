#include <iostream>
#include <utility>

template <typename T, T... Nums>
void foo(std::integer_sequence<T, Nums...>)
{
    for (auto i: {Nums...})
    {
        std::cout<<i<<"\n";
    }
}

int main()
{
    auto identity = [] (int n) constexpr { return n; };
    static_assert(identity(123) == 123);

    int a[2] = {1,2};

    auto [x,y] = a;

    std::cout<<x<<"\n";

    auto seq =  std::make_integer_sequence<int, 5>();

    foo(seq);

}
